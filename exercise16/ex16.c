#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

struct Person {
    char *name;
    int age;
    int height; // cms
    int weight; // kg
};

struct Person *Person_create(char *name, int age, int height, int weight)
{
    struct Person *who = malloc(sizeof(struct Person));
    assert(who != NULL);

    who->name = strdup(name);
    who->age = age;
    who->height = height;
    who->weight = weight;

    return who;
}

void Person_destroy(struct Person *who)
{
    assert(who != NULL);

    free(who->name);
    free(who);
}

void Person_print(struct Person *who)
{
    printf("Name: %s\n", who->name);
    printf("\tAge: %d\n", who->age);
    printf("\tHeight: %d\n", who->height);
    printf("\tWeight: %d\n", who->weight);
}
void Person_print_extra(struct Person who)
{
    printf("Name: %s\n", who.name);
    printf("\tAge: %d\n", who.age);
    printf("\tHeight: %d\n", who.height);
    printf("\tWeight: %d\n", who.weight);
}

int main(int argc, char *argv[])
{
    // make two people structures
    struct Person *joe = Person_create("Joe Puche", 22, 161, 61);
    struct Person *bea = Person_create("Bea Cat Lover", 19, 169, 56);

    // print them out and where they are in memoty
    printf("Joe is at memory location %p:\n", joe);
    Person_print(joe);

    printf("Bea is at memory location %p:\n", bea);
    Person_print(bea);

    // make everyone 20 years older and print again
    joe->age += 20;
    joe->height -= 2;
    joe->weight += 8;
    Person_print(joe);

    bea->age += 20;
    bea->weight += 8;
    Person_print(bea);

    // destroy them both
    Person_destroy(joe);
    Person_destroy(bea);

    // EXTRA CREDIT

    // creating a struct in the stack and initialize using .
    struct Person rene;
    rene.name = "Renè";
    rene.age = 25;
    rene.height = 177;
    rene.weight = 79;

    Person_print_extra(rene); // passing a struct to a function

    return 0;
}

