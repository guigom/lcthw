# Exercise 18 | Pointers to functions

## Extra credit

### Get a hex editor and open up ex18, find sequence of hex digits that start a function
After looking at the program's output
```
SORTED:55:48:89:e5:89:7d:fc:89:75:f8:8b:45:fc:2b:45:f8:5d:c3:55:48:89:e5:89:7d:fc:
REVERSED:55:48:89:e5:89:7d:fc:89:75:f8:8b:45:f8:2b:45:fc:5d:c3:55:48:89:e5:89:7d:fc:
```

I've used **Vim** and **xxd** to inspect the binary and after some searching I found

```
                                v
00001350: ffff ff48 8b45 f8c9 c355 4889 e589 7dfc  ...H.E...UH...}.
00001360: 8975 f88b 45fc 2b45 f85d c355 4889 e589  .u..E.+E.].UH...
00001370: 7dfc 8975 f88b 45f8 2b45 fc5d c355 4889  }..u..E.+E.].UH.
             ^
```

### Find other random things and change them. Rerun the program and see

#### Finding some strings

Searched for strings like "SORTED:" and "REVERSED:" found this interesting
piece. Just after a big chunk of 0's starting at
```
000016d0: 0000 0000 0000 0000 0000 0000 0000 0000  ................
...
00001ff0: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00002000: 0100 0200 0000 0000 4552 524f 523a 2025  ........ERROR: %
00002010: 730a 0000 0000 0000 4d65 6d6f 7279 2065  s.......Memory e
00002020: 7272 6f72 2061 6c6c 6f63 6174 696e 6720  rror allocating 
00002030: 7461 7267 6574 2066 6f72 2062 7562 626c  target for bubbl
00002040: 6520 736f 7274 0046 6169 6c65 6420 746f  e sort.Failed to
00002050: 2073 6f72 7420 6173 2072 6571 7565 7374   sort as request
00002060: 6564 0025 6420 0025 3032 783a 0055 5341  ed.%d .%02x:.USA
00002070: 4745 3a20 6578 3138 2034 2033 2031 2035  GE: ex18 4 3 1 5
00002080: 2036 0000 0000 0000 4d65 6d6f 7279 2065   6......Memory e
00002090: 7272 6f72 2061 6c6c 6f63 6174 696e 6720  rror allocating 
000020a0: 6e75 6d62 6572 7320 6174 206d 6169 6e00  numbers at main.
000020b0: 534f 5254 4544 3a00 5245 5645 5253 4544  SORTED:.REVERSED
```

All character strings used in the program seems to be stored just there.
Interesting, unfortunately right now I have no knowledge why is it that way.

### Pass in the wrong function for the `compare_cb` and see C complain about it

If we put an incorrect function pointer, meaning that it has different
prototype than its declaration the compiler will notice and fail to compile
telling us about that error.

If I pass main function instead of one of the sorting methods I get:

> ex18.c|75 col 55 error| note: expected ‘compare_cb’ {aka ‘int (*)(int,  int)’} but argument is of type ‘int (*)(int,  char **)’

### Pass in NULL

If I run `./ex18 NULL` I get the following output, but that's just atoi trying
to convert 'N' to an int and count being initialized to one as the string
'NULL' count as one argument
```
0 
0 
0 
SORTED:55:48:89:e5:89:7d:fc:89:75:f8:8b:45:fc:2b:45:f8:5d:c3:55:48:89:e5:89:7d:fc:
REVERSED:55:48:89:e5:89:7d:fc:89:75:f8:8b:45:f8:2b:45:fc:5d:c3:55:48:89:e5:89:7d:fc:
```

Things get funnier if we pass NULL in the compare_cb argument (in this example
I pass it to the third test_sorting call in line 140)
```
    test_sorting(numbers, count, NULL);
```
As expected, passing a NULL pointer is not a wise idea as the program fails.
Trying to get a backtrace with `gdb --batch --ex r --ex bt --ex q --args ./ex18 5 1 3
2`

Gives the following output

```
1 2 3 5 
5 3 2 1 

Program received signal SIGSEGV, Segmentation fault.
0x0000000000000000 in ?? ()
#0  0x0000000000000000 in ?? ()
#1  0x00005555555552c2 in bubble_sort (numbers=0x555555559260, count=4, cmp=0x0) at ex18.c:40
#2  0x00005555555553d4 in test_sorting (numbers=0x555555559260, count=4, cmp=0x0) at ex18.c:78
#3  0x00005555555555e8 in main (argc=5, argv=0x7fffffffe958) at ex18.c:140
A debugging session is active.

	Inferior 1 [process 8827] will be killed.

Quit anyway? (y or n) [answered Y; input not from terminal]
```

99% of the cases depending on system architecture trying to call a function
pointing to NULL will result in Segmentation Fault as it will try to execute
the function in the memory address that the macro NULL evaluates, in my case 0.

### Write another sorting algorithm, then change `test_sorting` so that it admits a sorting algorithm callback function

To do.
