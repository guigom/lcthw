#include "dbg.h"
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

/** Our old friend die from ex17 */
void die(const char *message)
{
    if (errno) {
        perror(message);
    } else {
        printf("ERROR: %s\n", message);
    }

    exit(1);
}

// a typedef creates a fake type, in this case
// for a function pointer
typedef int (*compare_cb) (int a, int b);

/**
 * A classic bubble sort function that uses the
 * compare_cb to do the sorting
 */
int *bubble_sort(int *numbers, int count, compare_cb cmp)
{
    int i = 0;
    int j = 0;
    int temp = 0;
    int *target = malloc(sizeof(int) * count);

    if(!target)
        die("Memory error allocating target for bubble sort");

    memcpy(target, numbers, sizeof(int) * count);

    for (i = 0; i < count; i++) {
        for (j = 0; j < count - 1 - i; j++) {
            if (cmp(target[j],target[j+1]) > 0) {
                temp = target[j];
                target[j] = target[j+1];
                target[j+1] = temp;
            }
        }
    }

    return target;
}

int sorted_order(int a, int b)
{
    return a - b;
}

int reversed_order(int a, int b)
{
    return b - a;
}

int strange_order(int a, int b)
{
    if (a == 0 || b == 0) {
        return 0;
    } else {
        return a % b;
    }
}


/**
 * Used to test that we are sorting things correctly
 * by doing the sort and printing it out
 */
void test_sorting(int *numbers, int count, compare_cb cmp)
{
    int i = 0;
    int *sorted = bubble_sort(numbers, count, cmp);

    if(!sorted)
        die("Failed to sort as requested");

    for (i = 0; i < count; i++) {
        printf("%d ", sorted[i]);
    }
    printf("\n");

    free(sorted);
}

/**
 * How to break it, dump method
 */
void dump(compare_cb cmp)
{
    int i = 0;
    unsigned char *data = (unsigned char *)cmp;

    for (i = 0; i < 25; i++) {
        printf("%02x:", data[i]);
    }

        printf("\n");
}

/**
 * This function can't work as it will try to write in write-protected
 * memory
 */
void destroy(compare_cb cmp)
{
    int i = 0;
    unsigned char *data = (unsigned char *)cmp;

    for (i = 0; i < 2; i++) {
        data[i] = i;
    }

        printf("\n");
}

int main(int argc, char *argv[])
{
    if (argc < 2) die("USAGE: ex18 4 3 1 5 6");

    int count = argc - 1;
    int i = 0;
    // Shift pointer one byte so we omit first char pointer to "ex18"
    char **inputs = argv + 1;

    int *numbers = malloc(sizeof(int) * count);
    if (!numbers) die("Memory error allocating numbers at main");

    for (i = 0; i < count; i++) {
        numbers[i] = atoi(inputs[i]);
    }

    test_sorting(numbers, count, sorted_order);
    test_sorting(numbers, count, reversed_order);
    test_sorting(numbers, count, strange_order);

    free(numbers);

    printf("SORTED:");
    dump(sorted_order);
    printf("REVERSED:");
    dump(reversed_order);

    return 0;
}
