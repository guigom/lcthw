* Exact width integers:
    - required:
        int{8,16,32,64*}_t
        uint{8,16,32,64*}_t
    - other N are considered optional

* Minimum width integers:
    - required:
        int_least{8,16,32,64*}_t
        uint_least{8,16,32,64*}_t
    - other N are considered optional

* Fastest minimum width integers:
    - required:
        int_fast{8,16,32,64*}_t
        uint_fast{8,16,32,64*}_t
    - other N are considered optional

* Integer types capable of holding object pointer
    - required on XSI-conformant systems, otherwise optional:
        intptr_t
        uintptr_t

* Greatest-width integer types
    - required
        intmax_t
        uintmax_t
