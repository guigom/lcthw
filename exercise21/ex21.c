#include <stdint.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	printf("In my case, this an x86_64 computer. Max integers are expected to be 8 bytes.\n");

	printf("stdint.h exercise:\n\n");	
	printf("--- uintmax_t ---\n");	
	printf("size: %lu byte\n", sizeof(uintmax_t));	
	printf("uintmax_t max value: %lu\n", UINTMAX_MAX);

	printf("--- intmax_t ---\n");	
	printf("size of largest possible int on this system: %lu\n", sizeof(intmax_t));
	
	printf("--- uint32_t ---\n");	
	printf("size: %lu byte\n", sizeof(uint32_t));	
	printf("uint32_t max value: %u\n", UINT32_MAX);

	printf("--- uint16_t ---\n");	
	printf("size: %lu byte\n", sizeof(uint16_t));	
	printf("uint16_t max value: %u\n", UINT16_MAX);

	printf("--- uint8_t ---\n");	
	printf("size: %lu byte\n", sizeof(uint8_t));	
	printf("uint8_t max value: %u\n", UINT8_MAX);

	printf("--- uint_least8_t ---\n");	
	printf("size: %lu byte\n", sizeof(uint_least8_t));	
	printf("uint_least8_t max value: %u\n", UINT_LEAST8_MAX);

	printf("--- uint_fast8_t ---\n");	
	printf("size: %lu byte\n", sizeof(uint_fast8_t));	
	printf("uint_fast8_t max value: %u\n", UINT_FAST8_MAX);

	printf("--- size_t ---\n");	
	printf("size: %lu byte\n", sizeof(size_t));	
	printf("size_t max value: %lu\n", SIZE_MAX);

}
