# Exercise 19 | Zed's Debug Macros

Introduction to the C preprocessor by analyzing some debug macros shown in
the book, focusing on variadic expression inside them in order to understand
that CPP is a recursive templating program.

## Extra credit

### Passing NDEBUG inside the Makefile

This extra credit is about not needing to put `#define NDEBUG` inside `ex19.c`.

Taking a look at `man 5 makefiles`:

```
CPPFLAGS    The  flags  that are used with the c-preprocessor.  This macro as well as: CPPOPTS and CPPOPTX are also used when compiling c-programs.  They should
            contain only the following flags: -Dname=value , -Uname , -Idirectory and -Ydirectory.
            Do not change this macro.
```

We can pass flags to the C preprocessor in the Makefile by using the 
`CPPFLAGS` predefined macro for the make rule, using `-D` to define `NDEBUG`,
there is no value needed.

```
CPPFLAGS=-DNDEBUG
CFLAGS=-Wall -g
TARGET=ex19

all: clean $(TARGET)

clean:
	rm -f $(TARGET)
```

### My Makefile

In this case I am not declaring `clean` as a dependency for `all` as this
defeats the purpose of `make` to detect that a dependency has changed or not
to detect if compiling is necessary or not.

I want to define `NDEBUG` only when running the `release`, for that I've used
[target
specific variables](https://www.gnu.org/software/make/manual/html_node/Target_002dspecific.html)

```
CC=gcc
CFLAGS=-Wall -g
TARGET=ex19

all: $(TARGET)

release: CPPFLAGS=-DNDEBUG
release: $(TARGET)

$(TARGET): $(TARGET).c
	$(CC) $(CFLAGS) $(CPPFLAGS) $(TARGET).c -o $(TARGET)

clean:
	rm -f $(TARGET)
```
