#include <stdio.h>

/* This is a comment. */
int main(int argc, char *argv[])
{
    int distance = 100;
    char initial = 'j';
    int numbers[] = { 1, 2, 3};
    double div = 3.0/2.0;
    char string[] = "Hard";

    // this is also a comment
    printf("You are %d miles away.\n", distance);
    // Print directions of the pointer?
    printf("Numbers: %p\n" ,numbers);
    printf("Dos: %d\n", numbers[1]);
    printf("My name starts with %c\n", initial);
    printf("3 / 2 = %.2f\n", div);
    printf("This book is %s\n", string);

    return 0;
}
