#include <stdio.h>
#include <stdlib.h> // Extra credit, read about what NULL is

// If we read man page of stddef.h:
// Null pointer constant. Macro shell that expands to an integer constant expression with the value 0 cast to type void *.

int main(int argc, char *argv[])
{
    int i = 0;
    int j = 0;

    // Extra credit, try assigning something to argv
    // Note: You can also assign elements from states if the array is declared before
    argv[1] = "boop.";

    for (i = 1; i < argc; i++){
        printf("arg %d: %s\n", i, argv[i]);
    }

    char *states[] = {
        "California", "Oregon",
        "Washington", "Texas", NULL, 0
    };

    // Extra credit, try assigning values of argv to states
    states[0] = argv[0];

    int num_states = 6;

    for (i = 0; i < num_states; i++) {
        printf("state %d: %s\n", i, states[i]);
    }

    // Extra credit, investigate how to use the comma character to separate multiple statements,
    // see what statements can you run inside a for-loop
    for (i = 10, j = 0; i > j; i--,j++,printf("hola\n")) {
        printf("i: %d | j: %d\n", i, j);
    }

    return 0;
}
