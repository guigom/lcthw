#include <stdio.h>

/*
 * 8 byte alignment/memory word on my machine
 */

// OUTPUT: 8
struct stu_a {
    int i;  // 4 bytes
    char c; // 1 byte
};

// OUTPUT: 16
struct stu_b {
    long i; // 8 bytes
    char c; // 1 byte
};

// OUTPUT: 16
struct stu_c {
    int i;  // 4 bytes
    char c; // 1 byte
    long d; // 8 bytes
};

// OUTPUT: 24
struct stu_d {
    long i; // 8 bytes
    char c; // 1 byte
    long d; // 8 bytes
};

// OUTPUT: 40 ( 1(8) + 24 + 8 = 40 )
//                ^ padding
struct stu_e {
    char c; // 1 byte
    struct stu_d s;
    long d; // 8 bytes
};

int main(int argc, char *argv[])
{
    printf("Size of struct #1: %ld\n", sizeof(struct stu_a));
    printf("Size of struct #2: %ld\n", sizeof(struct stu_b));
    printf("Size of struct #3: %ld\n", sizeof(struct stu_c));
    printf("Size of struct #4: %ld\n", sizeof(struct stu_d));
    printf("Size of struct #5: %ld\n", sizeof(struct stu_e));
}
