#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <bsd/string.h>

#define MAX_DATA 512
#define MAX_ROWS 100

struct Address {
    int id;
    int set;
    char name[MAX_DATA];
    char email[MAX_DATA];
};

struct Database {
    struct Address rows[MAX_ROWS];
};

struct Connection {
    FILE *file;
    struct Database *db;
};

// Forward declaration
void Database_close(struct Connection *conn);

/*
 * Extra credit: Improve die so it receives a Connection to clean up before
 * exiting
 */
void die(const char *message, struct Connection *conn)
{
    if(errno) {
        perror(message);
    } else {
        printf("ERROR: %s\n", message);
    }

    Database_close(conn);

    exit(1);
}

void Address_print(struct Address *addr)
{
    printf("%d %s %s\n", addr->id, addr->name, addr->email);
}

void Database_load(struct Connection *conn)
{
    int rc = fread(conn->db, sizeof(struct Database), 1, conn->file);
    if (rc != 1)
        die("Failed to load the database.", conn);
}

struct Connection *Database_open(const char *filename, char mode)
{
    struct Connection *conn = malloc(sizeof(struct Connection));
    if (!conn)
        die("Memory error", conn);

    conn->db = malloc(sizeof(struct Database));
    if (!conn->db)
        die("Memory error", conn);

    if (mode == 'c') {
        conn->file = fopen(filename, "w");
    } else {
        conn->file = fopen(filename, "r+");

        if(conn->file) {
            Database_load(conn);
        }
    }

    if (!conn->file) {
        die("Failed to open the file", conn);
    }

    return conn;
}

void Database_close(struct Connection *conn)
{
    if(conn) {
        if (conn->file)
            fclose(conn->file);
        if (conn->db)
            free(conn->db);
        free(conn);
    }
}

void Database_create(struct Connection *conn)
{
    int i = 0;

    for (i = 0; i < MAX_ROWS; i++) {
        // make a prototype row(Address)
        struct Address addr = {.id = i, .set = 0};
        // then just assign it
        conn->db->rows[i] = addr;
    }
}

void Database_write(struct Connection *conn)
{
    rewind(conn->file);

    // fwrite returns size_t
    int rc = fwrite(conn->db, sizeof(struct Database), 1, conn->file);
    if (rc != 1)
        die("Failed to write the database.", conn);

    // fflush returns EOF in case of error and sets errno
    rc = fflush(conn->file);
    if(rc == -1) {
        die("Cannot flush database", conn);
    }
}

void Database_set(struct Connection *conn, int id, const char *name, const char
        *email)
{
    struct Address *addr = &conn->db->rows[id];
    if (addr->set)
        die("Already set, delete it first.", conn);

    addr->set = 1;

//    int res2 = strlcpy(addr->name, name, MAX_DATA);
//    printf("strlcpy bytes copied: %d | expected %d", res2, MAX_DATA);
    // WARNING: bug, read the "How to break it" and fix this
    char *dup = strdup(name);
    dup[MAX_DATA-1] = '\0';
    char *res = strncpy(addr->name, dup, MAX_DATA);
    // demonstrate the strncopy bug
    if (!res)
        die("Name copy failed.", conn);

    res = strncpy(addr->email, email, MAX_DATA);
    if (!res)
        die("Email copy failed.", conn);

//    struct Address addr = { .name = name, .email = email };
}

void Database_get(struct Connection *conn, int id)
{
    struct Address *addr = &conn->db->rows[id];

    if (addr->set) {
        Address_print(addr);
    } else {
        die("Address with that ID is not set.\n", conn);
    }
}

void Database_list(struct Connection *conn)
{
    int i = 0;
    struct Database *db = conn->db;

    for (i=0; i < MAX_ROWS; i++) {
        struct Address *cur = &db->rows[i];

        if (cur->set) {
            Address_print(cur);
        }
    }
}

void Database_delete(struct Connection *conn, int id)
{
    struct Address addr = { .id = id, .set = 0};
    conn->db->rows[id] = addr;
}

int main(int argc, char *argv[])
{
    if (argc < 3)
        die("USAGE: ex17 <dbfile> <action> [action params]", NULL);

    // Get param values
    char *filename = argv[1];
    char action = argv[2][0];
    struct Connection *conn = Database_open(filename, action);
    int id = 0;

    // First param is always going to be row id
    if (argc > 3) id = atoi(argv[3]);
    if (id >= MAX_ROWS) die("There are not that many records", conn);

    switch (action) {
        case 'c':
            Database_create(conn);
            Database_write(conn);
            break;

        case 'g':
            if (argc != 4)
                    die("Need an id to get.", conn);

            Database_get(conn, id);
            break;

        case 's':
            if (argc != 6)
                die("ex17 <filename> s <id> <name> <email>\n", conn);

            Database_set(conn, id, argv[4], argv[5]);
            Database_write(conn);
            break;

        case 'd':
            if (argc != 4)
                    die("Need an id to get.", conn);

            Database_delete(conn, id);
            Database_write(conn);
            break;

        case 'l':
            Database_list(conn);
            break;

        default:
            die("Invalid action: c=create, g=get, s=set, d=del, l=list", conn);
    }

    Database_close(conn);

    return 0;
}
