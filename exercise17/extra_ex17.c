/*
 * Extra credit, receive MAX_DATA/ROWS when creating a database as arguments.
 *
 * I keep some default value defined just becuase it seemed like a nice idea
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <bsd/string.h>

#define DEFAULT_MAX_DATA 512
#define DEFAULT_MAX_ROWS 100

struct Address {
    int id;
    int set;
    char *name;
    char *email;
};

struct Database {
    int MAX_ROWS;
    int MAX_DATA;
    struct Address **rows;
};

struct Connection {
    FILE *file;
    struct Database *db;
};

// Forward declaration
void Database_close(struct Connection *conn);

/*
 * Extra credit: Improve die so it receives a Connection to clean up before
 * exiting
 */
void die(const char *message, struct Connection *conn)
{
    if(errno) {
        perror(message);
    } else {
        printf("ERROR: %s\n", message);
    }

    Database_close(conn);

    exit(1);
}

void Address_print(struct Address *addr)
{
    printf("%d %s %s\n", addr->id, addr->name, addr->email);
}

void Database_get(struct Connection *conn, int id)
{
    struct Address *addr = conn->db->rows[id];

    if (addr->set) {
        Address_print(addr);
    } else {
        die("Address with that ID is not set.\n", conn);
    }
}

void Database_load(struct Connection *conn)
{
    int i = 0;

    // Read MAX_ROWS
    int rc = fread(&conn->db->MAX_ROWS, sizeof(int), 1, conn->file);
    if (rc != 1)
        die("Failed to load the database MAX_ROWS", conn);
    // Read MAX_DATA
    rc = fread(&conn->db->MAX_DATA, sizeof(int), 1, conn->file);
    if (rc != 1)
        die("Failed to load the database MAX_DATA", conn);

    // Before reading rows we need to allocate the necessary memory given
    // MAX_ROWS value read from the database file
    conn->db->rows = malloc(sizeof(struct Address*) * conn->db->MAX_ROWS);

    // Read all rows
    for (i=0; i<conn->db->MAX_ROWS; i++) {
        struct Address *row_ptr = malloc(sizeof(struct Address));
        // Read id
        rc = fread(&row_ptr->id, sizeof(int), 1, conn->file);
        if (rc != 1)
            die("Failed to load row id", conn);

        // Read set
        rc = fread(&row_ptr->set, sizeof(int), 1, conn->file);
        if (rc != 1)
            die("Failed to load row set", conn);

        // Read name
        row_ptr->name = malloc(sizeof(char)*conn->db->MAX_DATA);
        rc = fread(row_ptr->name, conn->db->MAX_DATA, 1, conn->file);
        if (rc != 1)
            die("Failed to load row name", conn);

        // Read email
        row_ptr->email = malloc(sizeof(char)*conn->db->MAX_DATA);
        rc = fread(row_ptr->email, conn->db->MAX_DATA, 1, conn->file);
        if (rc != 1)
            die("Failed to load row email", conn);

        // Finally add this Address pointer
        conn->db->rows[i] = row_ptr;
    }

}

struct Connection *Database_open(const char *filename, char mode)
{
    struct Connection *conn = malloc(sizeof(struct Connection));
    if (!conn)
        die("Memory error", conn);

    conn->db = malloc(sizeof(struct Database));
    if (!conn->db)
        die("Memory error", conn);

    if (mode == 'c') {
        conn->file = fopen(filename, "w");
    } else {
        conn->file = fopen(filename, "r+");

        if(conn->file) {
            Database_load(conn);
        }
    }

    if (!conn->file) {
        die("Failed to open the file", conn);
    }

    return conn;
}

void Database_close(struct Connection *conn)
{
    int i = 0;
//    if(conn) {
//        if (conn->file)
//            fclose(conn->file);
//        if (conn->db)
//            free(conn->db);
//        free(conn);
//    }
    if(conn) {
        if (conn->file)
            fclose(conn->file);
        if (conn->db) {
            if(conn->db->rows) {
                for (i = 0; i < conn->db->MAX_ROWS; i++) {
                    struct Address *row_ptr = conn->db->rows[i];
                    free(row_ptr->name);
                    free(row_ptr->email);
                    free(row_ptr);
                }
                free(conn->db->rows);
            }
            free(conn->db);
        }
        free(conn);
    }
}

void Database_create(struct Connection *conn, int max_rows, int max_data)
{
    conn->db->MAX_ROWS = max_rows;
    conn->db->MAX_DATA = max_data;

    // Allocate the size of rows

    conn->db->rows = malloc(sizeof(struct Address) * max_rows);

    int i = 0;

    for (i = 0; i < max_rows; i++) {
        struct Address *row = malloc(sizeof(struct Address));
        row->id = i;
        row->set = 0;
        row->name = calloc(1, max_data);
        row->email = calloc(1, max_data);

        conn->db->rows[i] = row;
    }
}

void Database_write(struct Connection *conn)
{
    int i = 0;
    rewind(conn->file);

    // Write MAX_ROWS
    int rc = fwrite(&conn->db->MAX_ROWS, sizeof(int), 1, conn->file);
    if (rc != 1)
        die("Failed to write the database.", conn);
    // Write MAX_DATA
    rc = fwrite(&conn->db->MAX_DATA, sizeof(int), 1, conn->file);
    if (rc != 1)
        die("Failed to write the database.", conn);

    // Write all the rows
    for(i = 0; i < conn->db->MAX_ROWS; i++) {
        struct Address *row_ptr = conn->db->rows[i];

        // Write id
        rc = fwrite(&row_ptr->id, sizeof(int), 1, conn->file);
        if (rc != 1)
            die("Failed to write database row id", conn);

        // Write set
        rc = fwrite(&row_ptr->set, sizeof(int), 1, conn->file);
        if (rc != 1)
            die("Failed to write database row set", conn);

        // Write name
        rc = fwrite(row_ptr->name, sizeof(char)*conn->db->MAX_DATA, 1, conn->file);
        if (rc != 1)
            die("Failed to write database row name", conn);

        // Write email
        rc = fwrite(row_ptr->email, sizeof(char)*conn->db->MAX_DATA, 1, conn->file);
        if (rc != 1)
            die("Failed to write database row email", conn);
    }

    rc = fflush(conn->file);
    if(rc == -1) {
        die("Cannot flush database", conn);
    }
}

void Database_set(struct Connection *conn, int id, const char *name, const char
        *email)
{
    struct Address *addr = conn->db->rows[id];
    if (addr->set)
        die("Already set, delete it first.", conn);

    addr->set = 1;

// Here we can use strlcpy that NUL terminates automatically, see README
//    int res2 = strlcpy(addr->name, name, MAX_DATA);
//    printf("strlcpy bytes copied: %d | expected %d", res2, MAX_DATA);
    char *dup_name = strdup(name);
    dup_name[conn->db->MAX_DATA-1] = '\0';
    char *res = strncpy(addr->name, dup_name, conn->db->MAX_DATA);
    if (!res)
        die("Name copy failed.", conn);

    char *dup_email = strdup(email);
    dup_email[conn->db->MAX_DATA-1] = '\0';
    res = strncpy(addr->email, dup_email, conn->db->MAX_DATA);
    if (!res)
        die("Email copy failed.", conn);
}

void Database_delete(struct Connection *conn, int id)
{
    struct Address *row_ptr = conn->db->rows[id];
    row_ptr->set = 0;
}

void Database_list(struct Connection *conn)
{
    int i = 0;
    struct Database *db = conn->db;

    for (i=0; i < conn->db->MAX_ROWS; i++) {
        struct Address *cur = db->rows[i];

        if (cur->set) {
            Address_print(cur);
        }
    }
}

int main(int argc, char *argv[])
{
    if (argc < 3)
        die("USAGE: extra_ex17 <dbfile> <action> [action params]", NULL);

    int max_rows, max_data;
    // Get param values
    char *filename = argv[1];
    char action = argv[2][0];
    struct Connection *conn = Database_open(filename, action);

    // First param is always going to be row id
    //if (argc > 3) id = atoi(argv[3]);
    //if (id >= MAX_ROWS) die("There are not that many records", conn);
    int id;

    switch (action) {
        case 'c':
            if (argc == 5) {
                max_rows = atoi(argv[3]);
                max_data = atoi(argv[4]);
                printf("Creating Database file with max_rows: %d & max_data: %d\n", max_rows, max_data);
            } else {
                printf("No number of rows and data size specified, falling back to default...\n");
                max_rows = DEFAULT_MAX_ROWS;
                max_data = DEFAULT_MAX_DATA;
            }
            Database_create(conn, max_rows, max_data);
            Database_write(conn);
            break;
        case 's':
            if (argc != 6)
                die("ex17 <filename> s <id> <name> <email>\n", conn);

            id = atoi(argv[3]);
            Database_set(conn, id, argv[4], argv[5]);
            Database_write(conn);
            break;
        case 'l':
            Database_list(conn);
            break;
        case 'g':
            if (argc != 4)
                    die("Need an id to get.", conn);

            id = atoi(argv[3]);
            Database_get(conn, id);
            break;
        case 'd':
            if (argc != 4)
                    die("Need an id to get.", conn);

            id = atoi(argv[3]);
            Database_delete(conn, id);
            Database_write(conn);
            break;
        default:
            die("Invalid action: c=create, g=get, s=set, d=del, l=list", conn);
    }

    Database_close(conn);

    return 0;
}
