#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
    int numbers[4] = { 0 };
    char name[4] = { 'a' };

    // first, print them out raw
    printf("numbers: %d %d %d %d\n",
            numbers[0],numbers[1],numbers[2],numbers[3]);

    printf("name each: %c %c %c %c\n",
            name[0], name[1], name[2], name[3]);

    printf("name: %s\n", name);

    // setup the numbers
    numbers[0] = 1;
    numbers[1] = 2;
    numbers[2] = 3;
    numbers[3] = 4;

    // setup the name
    name[0] = 'Z';
    name[1] = 'e';
    name[2] = 'd';
    name[3] = '\0';

    //then print them out initialized
    printf("numbers: %d %d %d %d\n",
            numbers[0], numbers[1], numbers[2], numbers[3]);

    printf("name each: %c %c %c %c\n",
            name[0],name[1],name[2],name[3]);

    // print the name like a string
    printf("name: %s\n", name);

    // another way to use name
    char *another = "Jose";

    printf("another: %s\n", another);
    printf("another with %%d: %d\n", another);

    printf("another each: %c %c %c %c %c\n",
            another[0],another[1],another[2],another[3], another[4]);

    // EXTRA CREDIT

    // Assign characters into numbers
    numbers[0] = name[0];
    numbers[1] = name[1];
    numbers[2] = name[2];
    numbers[3] = name[3];
    printf("Printing numbers with chars using %%c: %c %c %c %c\n",
            numbers[0], numbers[1], numbers[2], numbers[3]);

    // Treat name as an integer array
    printf("Printing name chars as int: %d %d %d %d\n",
            name[0], name[1], name[2], name[3]);


    // Name is a 4 char(byte) which is sized as an integer, treat the
    // 4-char array as an integer

    // Casting the char pointer to int pointer and dereferencing it
    int name_int = *((int *)name);
    printf("name as int: %s\n",
            &name_int);

    // Using bit wise operators
    int kek2 = (name[0]<< 0) | (name[1] << 8) | (name[2] << 16) | (name[3] << 24);
    printf("name as int (shift): %s\n",
            &kek2);

    // Using memcpy
    int kek3;
    memcpy(&kek3, name, sizeof(int));
    printf("name as int (memcpy): %s\n",
            &kek3);

    // "convert" name to be in style of another (char pointer)
    // Note: You can't declare a char pointer and assign { 'a' } to it, it
    // does not make sense.
    char *name2 = "Jose";
    char c = name2[1];
    printf("name2[1] : %c", c);
}
