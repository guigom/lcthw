#include <stdio.h>
#include <ctype.h>
#include <string.h>

// forward declarations
int can_print_it(char ch);
void print_letters(char arg[], int len);
void print_digits(char arg[]);

void print_arguments(int argc, char *argv[])
{
    int i = 0;

    for (i = 0; i < argc; i++) {
    //Extra credit, figure out string length, pass it to print_letters, do not
    //rely on \0
        int len = strlen(argv[i]);
        print_letters(argv[i], len);
        print_digits(argv[i]);
    }
}

void print_letters(char arg[], int len)
{
    int i = 0;

    //for (i = 0; arg[i] != '\0'; i++) {
    // Extra credit
    for (i = 0; i < len; i++) {
        char ch = arg[i];

//        if(can_print_it(ch)) {
//        Extra credit
        if (isalpha(ch) || isblank(ch)) {
            printf("'%c' == %d ", ch, ch);
        }
    }

    printf("\n");
}

int can_print_it(char ch)
{
    return isalpha(ch) || isblank(ch);
}

// Extra credit. Read about isalpha, isblank, figure out how to print only
// digits.

void print_digits(char str[])
{
    printf("printing digits... ");
    int i = 0;

    for (i = 0; i < strlen(str); i++) {
        if(isdigit(str[i])) {
            printf("%c ", str[i]);
        }
    }
    printf("\n");
}

int main(int argc, char *argv[])
{
//  argc++;
    print_arguments(argc, argv);
    return 0;
}

